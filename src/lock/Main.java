package lock;

public class Main
{

	public static void main(String[] args)
	{

		// KeyLock a = new KeyLock(2);
		// a.insert(8);
		// a.turn();
		// a.unlock();
		// a.lock();
		//
		// ComboLock c = new ComboLock();
		// c.turnRight(c.myCombination[0]);
		// c.turnLeft(c.myCombination[1]);
		// c.turnRight(c.myCombination[2]);
		// c.unlock();
		// c.reset();
		// c.getIsReset();
		// c.lock();
		// c.turnRight(1);
		// c.turnLeft(2);
		// c.turnRight(3);
		// c.unlock();

		KeylessEntryLock b = new KeylessEntryLock(8);

		/*
		 * Create a new 4DUC.
		 */
		b.pushKey('1');
		b.pushKey('2');
		b.pushKey('3');
		b.pushKey('4');
		b.pushKey('5');
		b.pushKey('6');

		b.pushKey('*');
		b.pushKey('3');

		/*
		 * Make master passcode
		 */
		b.pushKey('6');
		b.pushKey('5');
		b.pushKey('4');
		b.pushKey('3');
		b.pushKey('2');
		b.pushKey('1');
		b.pushKey('6');
		b.pushKey('5');
		b.pushKey('4');
		b.pushKey('3');
		b.pushKey('2');
		b.pushKey('1');

		/*
		 * Add 4duc
		 */
		b.pushKey('6');
		b.pushKey('5');
		b.pushKey('4');
		b.pushKey('3');
		b.pushKey('2');
		b.pushKey('1');

		b.pushKey('*');
		b.pushKey('1');

		/*
		 * Make 4duc
		 */
		b.pushKey('1');
		b.pushKey('2');
		b.pushKey('3');
		b.pushKey('4');
		b.pushKey('1');
		b.pushKey('2');
		b.pushKey('3');
		b.pushKey('4');

		b.pushKey('1');
		b.pushKey('2');
		b.pushKey('3');
		b.pushKey('4');

		System.out.println(b.unlock());

	}
}
