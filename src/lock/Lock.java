package lock;

/**
 * @purpose
 * 
 * Lock interface allows all other types of locks to 
 * implement it, forcing them to have the listed methods.
 * 
 * @author Erica Kok
 * @author Chad Baily
 * 
 * @dueDate: 2/5/16
 */

public interface Lock
{
	public boolean isLocked();

	public boolean lock();

	public boolean unlock();

}