package lock;

/**
 * @purpose
 * 
 * KeyLock class simulates an actual key lock where a master key
 * is set once a new KeyLock is created. Users are able to input
 * a key and the program checks to see if the key entered is equal
 * to the master key before it can be unlocked.
 * 
 * @author Erica Kok
 * @author Chad Baily
 * 
 * @dueDate 2/10/16
 */

public class KeyLock implements Lock
{
	private int myKeyValue;

	private int myKey;

	protected boolean myIsLocked;

	private boolean myIsInserted;

	protected boolean myIsReadyToBeUnlocked;

	/**
	 * Constructor, sets the master key that opens the lock.
	 * 
	 * @param myKey
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public KeyLock(int key)
	{
		myKey = key;
		myIsReadyToBeUnlocked = false;
		myIsLocked = true;
	}

	/**
	 * Method to insert a key. The key entered must be > 0,
	 * otherwise you never inserted anything.
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public void insert(int keyValue)
	{
		myKeyValue = keyValue;
		if (keyValue >= 0)
		{
			myIsInserted = true;
		} else
		{
			myIsInserted = false;
		}
	}

	/**
	 * Method to turn the key you entered. You can only turn the key if 
	 * a key is inserted, and if you inserted the correct key.
	 * 
	 * @return true if a key was inserted and the key inserted is equal to the master key.
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public boolean turn()
	{
		if (myIsInserted && myKey == myKeyValue)
		{
			myIsReadyToBeUnlocked = true;
			return true;
		} else
		{
			return false;
		}

	}

	/**
	 * Method to check to see if key lock is locked.
	 * 
	 * @return true if key lock is locked, otherwise returns false
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public boolean isLocked()
	{
		if (myIsLocked == true)
		{
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Method to lock the key lock. Checks to see if key lock is 
	 * currently unlocked and it's not ready to be unlocked.
	 * 
	 * @return true if above conditions are met
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public boolean lock()
	{
		myIsLocked = true;
		myIsReadyToBeUnlocked = false;
		return true;
	}

	/**
	 * Method to unlock the lock. Checks to see if a key is inserted
	 * and if you turned the correct key.
	 * 
	 * @return true if it's ready to be unlocked
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public boolean unlock()
	{
		if (myIsReadyToBeUnlocked)
		{
			myIsLocked = false;
			return true;
		} else
		{
			return false;
		}
	}
}