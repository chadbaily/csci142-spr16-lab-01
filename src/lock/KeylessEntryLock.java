package lock;

/**
 * @purpose
 * 
 * 			KeylessEntryLock class that simulates the actions of a Keyless Entry
 *          Lock. The user must first input the default master passcode to enter
 *          a new passcode. New user codes can be added and deleted from the
 *          locks memory. The six digit passcode that the user created can be
 *          changed at any time at the users discretion. The lock must also be
 *          able to unlock via a key. This means that a key can be inserted and
 *          the lock must be able to unlock with a user code or a key at any
 *          time.
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 2/10/16
 */

public class KeylessEntryLock extends KeyLock
{

	public static final int MAX_NUM_USER_CODES = 10;
	public static final int USER_CODE_LENGTH = 4;
	public static final int MASTER_CODE_LENGTH = 6;

	private boolean myIsReset;
	private boolean myIsNewUserCode;
	private boolean myIsDeletedUserCode;
	private boolean myIsChangedMasterCode;
	private boolean myAllUserCodesDeleted;

	private int[] myMasterCode = new int[MASTER_CODE_LENGTH];
	private int[][] myUserCodes = new int[MAX_NUM_USER_CODES][USER_CODE_LENGTH];
	private int[] myDefaultMasterCode = new int[MASTER_CODE_LENGTH];
	private int[] myAttempt = new int[20];
	private int myPushCounter = 0;
	private int myUserCodeCounter = 0;

	/**
	 * Creates a new Keyless Entry Lock, with the master passcode of 1,2,3,4,5,6
	 * 
	 * @param key
	 */

	public KeylessEntryLock(int key)
	{
		super(key);
		myIsLocked = true;
		for (int i = 0; i < MASTER_CODE_LENGTH; i++)
		{
			myDefaultMasterCode[i] = i + 1;
		}
	}

	/**
	 * This method tests to see if you have pushed a valid key on the number
	 * pad. If you have then it compares the numbers you have entered to see if
	 * the user has entered a valid sequence. If they have then either a new
	 * passcode is created, a passcode is deleted, all passcodes are deleted, or
	 * the six digit passcode is changed.
	 * 
	 * @param key
	 * @return boolean
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean pushKey(char key)
	{
		myIsReset = false;
		myIsNewUserCode = false;
		myIsDeletedUserCode = false;
		myIsChangedMasterCode = false;
		myAllUserCodesDeleted = false;
		myIsReadyToBeUnlocked = false;

		/*
		 * Checks if key pressed is between 0-9 or *, then saves the key entered
		 * to myAttempt[].
		 */
		if (key >= '0' && key <= '9' || key == '*')
		{
			myAttempt[myPushCounter] = key;
			myPushCounter++;

		}
		else
		{
			return false;
		}

		/*
		 * Checks to see if myAttempt[] == myDefaultMasterCode[].
		 */
		if (this.userCodeChecker())
		{
			myIsReadyToBeUnlocked = true;
			myIsLocked = false;

		}

		if (this.isMasterPasscode())
		{
			/*
			 * If yes, then check to see if next thing entered is '*'.
			 */

			if ((char) myAttempt[6] == '*')
			{
				/*
				 * If yes, check to see if next thing entered is either 1, 2, 6,
				 * or 3. If '1' is entered:
				 */
				
				if (this.myAttemptEqualsOne())
				{

				}

				if (this.myAttemptEqualsTwo())
				{

				}
				/*
				 * Else if you enter '6':
				 */

				if (this.myAttemptEqualsSix())
				{

				}

				/*
				 * Else if you enter '3':
				 */

				if (this.myAttemptEqualsThree())
				{

				}
				
			}
		}
		return true;
	}

	/**
	 * Method to check to see if the next attempt is 1. If yes, check to see if attempt is a number,
	 * then check to see if it's a new user code.
	 * 
	 * @return true if all conditions above are met
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	
	private boolean myAttemptEqualsOne()
	{
		if ((char) myAttempt[7] == '1')
		{
			/*
			 * Check to see if the next 4 things entered is any number between
			 * 0-9.
			 */

			if (this.myAttemptIsNumber())

			{
				/*
				 * If yes, check to see if the next 4 numbers entered == the
				 * last 4 numbers entered.
				 */

				if (this.myAttemptEqualNewUserCode())

				{
					/*
					 * If yes, then add the 4 numbers just entered to
					 * myUserCodes[][].
					 */

					myUserCodes[myUserCodeCounter][0] = (char) myAttempt[8];
					myUserCodes[myUserCodeCounter][1] = (char) myAttempt[9];
					myUserCodes[myUserCodeCounter][2] = (char) myAttempt[10];
					myUserCodes[myUserCodeCounter][3] = (char) myAttempt[11];
					if (myPushCounter == 16)
					{
						myIsNewUserCode = true;
						myUserCodeCounter++;
						resetMyAttempt();
						myIsReset = true;
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Method to check to see if the next attempt is 2. If yes, then check to see if
	 * the next 4 numbers entered are part of the user code, then check to see if the
	 * same 4 numbers was entered, then delete that user code. 
	 * 
	 * @return
	 */
	private boolean myAttemptEqualsTwo()
	{
		if ((char) myAttempt[7] == '2')
		{
			/*
			 * Check to see if the next 4 numbers entered is part of
			 * myUserCodes[][].
			 */

			if (this.deleteUserCodeChecker())

			{
				/*
				 * If yes, check to see if the next 4 numbers entered == the
				 * last 4 numbers entered.
				 */

				if (this.myAttemptEqualNewUserCode())

				{
					/*
					 * If yes, set the place of those numbers in myUserCodes[][]
					 * = null so that they will be deleted. K cool.
					 * 
					 */
					int c = userCodeCheckerMatch();
					myUserCodes[c][0] = 0;
					myUserCodes[c][1] = 0;
					myUserCodes[c][2] = 0;
					myUserCodes[c][3] = 0;
					if (myPushCounter == 16)
					{
						myIsDeletedUserCode = true;
						resetMyAttempt();
						myIsReset = true;
						return true;
					}

				}
				else if (myPushCounter == 16)
				{
					resetMyAttempt();
				}
			}
			else if (myPushCounter == 16)
			{
				resetMyAttempt();
			}
		}
		return false;
	}
	
	/**
	 * Method to check if the next attempt is 3. If yes, check  to see if next 
	 * number entered is a passcode. If yes, then change the passcode currently
	 * set to the one that was entered.
	 *  
	 * @return true if all above conditions are met
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private boolean myAttemptEqualsThree()
	{
		if ((char) myAttempt[7] == '3')
		{
			/*
			 * Check to see if myAttempt at [8-13] != myAttempt at [0-5].
			 */

			if (myAttemptIsNumber())

			{
				/*
				 * If yes, check to see if myAttempt at [8-13] == myAttempt at
				 * [14-19], which basically checks to see if the new 6 numbers
				 * you entered == the last 6 numbers you entered. Awesome.
				 */

				if (myAttemptEqualNewPasscode())
				{
					/*
					 * If yes, then make the last 6 numbers you entered =
					 * myMasterCode. Yes.
					 */

					myMasterCode[0] = (char) myAttempt[8];
					myMasterCode[1] = (char) myAttempt[9];
					myMasterCode[2] = (char) myAttempt[10];
					myMasterCode[3] = (char) myAttempt[11];
					myMasterCode[4] = (char) myAttempt[12];
					myMasterCode[5] = (char) myAttempt[13];
					if (myPushCounter == 20)
					{
						myIsChangedMasterCode = true;
						resetMyAttempt();
						myIsReset = true;
						return true;
					}

				}
			}
		}
		return false;
	}
	
	/**
	 * Method to check to see if the next attempt is 6. If yes, then check 
	 * to see if the next numbers entered is the passcode. If yes, make sure
	 * the same passcode is entered again, then delete all the user codes.
	 * 
	 * @return true if all above conditions are met
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */

	private boolean myAttemptEqualsSix()
	{
		if ((char) myAttempt[7] == '6')
		{
			/*
			 * Check to see if the next 6 numbers entered == to the first 6
			 * numbers entered.
			 */

			if (this.isMasterPasscode())

			{
				/*
				 * If yes, then delete everything in myUserCode[][].
				 */
				if (myPushCounter == 14)
				{
					myUserCodes = new int[MAX_NUM_USER_CODES][USER_CODE_LENGTH];
					myAllUserCodesDeleted = true;
					resetMyAttempt();
					myIsReset = true;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method to reset the array myAttempt. Also sets the variable myPushCounter
	 * = 0, this completely resets everything
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public void resetMyAttempt()
	{
		for (int i = 0; i < myAttempt.length; i++)
		{
			myAttempt[i] = 0;
		}
		myPushCounter = 0;
	}

	/**
	 * Method to check if the sequence entered is part of a Master Passcode or a
	 * user created Passcode. The method is set-up so that it can decide which
	 * part needs to be tested (myDefaultMasterCode or myMasterCode)
	 * 
	 * @return if the sequence entered was part of a default master passcode or
	 *         a user master passcode, via a boolean
	 * @author Chad Baily
	 * @author Erica Kok
	 */

	private boolean isMasterPasscode()
	{
		int x = 0;
		boolean match = true;

		for (int i = 0; i < KeylessEntryLock.MASTER_CODE_LENGTH; i++)
		{
			match &= (int) myAttempt[i] == myDefaultMasterCode[i] + 48;
			if (match)
			{
				x++;
			}
			if (x == 6)
			{
				break;
			}
		}

		if (x == 6)
		{
			return match;
		}
		match = true;
		for (int i = 0; i < KeylessEntryLock.MASTER_CODE_LENGTH; i++)
		{
			match &= (int) myAttempt[i] == myMasterCode[i];
			if (match)
			{
				x++;
			}
			if (x == 6)
			{
				break;
			}
		}

		return match;

	}

	/**
	 * Method to check and see if what the user has entered is within an
	 * acceptable range for the predefined conditions.
	 * 
	 * @return
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private boolean myAttemptIsNumber()
	{
		boolean match = true;
		for (int i = 8; i < KeylessEntryLock.USER_CODE_LENGTH; i++)
		{
			match &= ((char) myAttempt[i] >= '0' && (char) myAttempt[i] <= '9');
		}
		return match;
	}

	/**
	 * Method to test if the 4duc that was just entered matches what the user
	 * wants is to be (previous four numbers entered)
	 * 
	 * @return a boolean, true if what the user entered the same 4duc twice,
	 *         false if the user did not enter the same numbers twice
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private boolean myAttemptEqualNewUserCode()
	{
		boolean match = true;
		for (int i = 0; i < KeylessEntryLock.USER_CODE_LENGTH; i++)
		{
			match &= ((char) myAttempt[i + 8] == (char) myAttempt[i + 12]);
		}
		return match;
	}

	/**
	 * Method to test if the 6dpc in the sequence was entered twice correctly
	 * 
	 * @return true if the 6dpc match, false if they do not
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private boolean myAttemptEqualNewPasscode()
	{
		boolean match = true;
		for (int i = 0; i < KeylessEntryLock.MASTER_CODE_LENGTH; i++)
		{
			match &= ((char) myAttempt[i + 8] == (char) myAttempt[i + 14]);
		}
		return match;
	}

	/**
	 * Method to test if the 4duc that was just entered is part of the usercodes
	 * 
	 * @return true if the 4duc entered is part of the usercodes, false if the
	 *         4duc entered is not part of the library of usercodes
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private boolean userCodeChecker()
	{
		int x = 0;
		boolean match = true;
		for (int i = 0; i < MAX_NUM_USER_CODES; i++)
		{
			match = true;
			for (int j = 0; j < KeylessEntryLock.USER_CODE_LENGTH; j++)
			{
				match &= ((char) myAttempt[j] == myUserCodes[i][j]);
				if (match)
				{
					x++;
				}
				if (x == 4)
				{
					break;
				}
			}
			if (x == 4)
			{
				break;
			}
		}
		return match;
	}

	/**
	 * Method to test if the usercode that the user entered is part of the
	 * library of usercodes. This is different from UserCodeChecker in that 8
	 * must be added to my attempt to account for the reset that occurred.
	 * Second UserCode checker for deleting a user code
	 * 
	 * @return
	 * @author Chad Baily
	 * @author Erica Kok
	 * 
	 */
	private boolean deleteUserCodeChecker()
	{
		int x = 0;
		boolean match = true;
		for (int i = 0; i < MAX_NUM_USER_CODES; i++)
		{
			match = true;
			for (int j = 0; j < KeylessEntryLock.USER_CODE_LENGTH; j++)
			{
				match &= ((char) myAttempt[j + 8] == myUserCodes[i][j]);
				if (match)
				{
					x++;
				}
				if (x == 4)
				{
					break;
				}
			}
			if (x == 4)
			{
				break;
			}
		}
		return match;
	}

	/**
	 * Method to test and see where the usercode matches, this will allow for
	 * the program to know where to delete the usercode, instead of not knowing.
	 * 
	 * @return returns where the usercode matches
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	private int userCodeCheckerMatch()
	{
		int x = 0;
		int i;
		boolean match = true;
		for (i = 0; i < myUserCodes.length; i++)
		{
			match = true;
			for (int j = 0; j < KeylessEntryLock.USER_CODE_LENGTH; j++)
			{
				match &= ((char) myAttempt[j + 8] == myUserCodes[i][j]);
				if (match)
				{
					x++;
				}
				if (x == 4)
				{
					break;
				}
			}
			if (x == 4)
			{
				break;
			}
		}
		return i;
	}

	/**
	 * Method to see if a new 4duc was added to the library of usercodes
	 * 
	 * @return true if a myIsNewUserCode is true, false if myIsNewUserCode is
	 *         false.
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean addedUserCode()
	{
		if (myIsNewUserCode)
		{
			return true;
		}
		return false;
	}

	/**
	 * Method to test if a 4duc was deleted from the library of usercodes
	 * 
	 * @return Whether myIsDeletedUserCode is true or false
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean deletedUserCode()
	{
		if (myIsDeletedUserCode)
		{
			return true;
		}
		return false;
	}

	/**
	 * Method to test if all of the usercodes have been cleared
	 * 
	 * @return Whether myAllUserCodesDeleted is true or false
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean clearedAllUserCodes()
	{
		if (myAllUserCodesDeleted)
		{
			return true;
		}
		return false;
	}

	/**
	 * Method to test if the master passcode was changed
	 * 
	 * @return Whether myIsChangedMasterCode is true or false
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean changedMasterCode()
	{
		if (myIsChangedMasterCode)
		{
			return true;
		}
		return false;
	}

	/**
	 * Method that returns the default master code
	 * 
	 * @return myDefaultMasterCode to the user
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public int[] getDefaultMasterCode()
	{
		return myDefaultMasterCode;
	}

	/**
	 * Method to lock the Keyless Entry Lock, had to reimplement it from KeyLock
	 * because some of the values needed were not able to be seen in KeyLock
	 * 
	 * @author Chad Baily
	 * @author Erica Kok
	 */
	public boolean lock()
	{
		myIsLocked = true;
		myIsReadyToBeUnlocked = false;
		resetMyAttempt();
		return true;
	}

}