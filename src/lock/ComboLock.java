package lock;

/**
 * @purpose 
 * 
 * ComboLock class simulates the actions of a real-life combo lock
 * where it comes with a set random combination and users can turn
 * the lock to the right and left and it only unlocks when the 
 * correct combination was entered.
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 2/10/16
 * 
 */

import java.util.Random;

public class ComboLock implements Lock
{

	public static final int MAX_NUMBER = 50;

	protected int[] myCombination = new int[3];

	private int[] myAttempt = new int[3];

	private boolean myIsLocked = true;

	private boolean myIsReset = false;

	private int myTurns = 1; // What the turn should be.

	private int myActualTurned = 0; // What the turn actually is on.

	/**
	 * Default constructor, does not let the user set the code. 
	 * The code can have numbers between 0 and MAX_NUMBER (50),
	 * and it should pass the conditions given.
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */
	public ComboLock()
	{
		Random rand = new Random();
		myCombination[0] = rand.nextInt(MAX_NUMBER - 0 + 1) + 0;
		int remainder = myCombination[0] % 4;
		int lastNumber = rand.nextInt(MAX_NUMBER - 0 + 1) + 0;
		int middleNumber = rand.nextInt(MAX_NUMBER - 0 + 1) + 0;
		while (true)
		{
			if (((middleNumber % 4 == ((remainder + 2) % 4))))
			{
				myCombination[1] = middleNumber;
				break;
			}
			else
			{
				middleNumber = rand.nextInt(MAX_NUMBER - 0 + 1) + 0;
			}
		}
		while (true)
		{
			if (((lastNumber % 4) == remainder))
			{
				myCombination[2] = lastNumber;
				break;
			}
			else
			{
				lastNumber = rand.nextInt(MAX_NUMBER - 0 + 1) + 0;
			}
		}

	}

	/**
	 * Method to allow the user to turn the dial to the right. 
	 * Keeps track of what turn it is on, can be reset with reset();
	 * 
	 * @param number
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public void turnRight(int number)
	{
		myIsReset = false;
		if (myTurns == 1)
		{
			if (number == myCombination[0])
			{
				myAttempt[0] = number;
				myIsLocked = true;
				myActualTurned = 1;
			}
		} 
		else if (myCombination[0] == myAttempt[0] && myTurns == 3 && myActualTurned == 2)
		{
			if (number == myCombination[2])
			{
				myAttempt[2] = number;
				myIsLocked = true;
				myActualTurned = 3;
			}
		}
	}

	/**
	 * Method to allow the user to turn the dial to the left. 
	 * Keeps track of what turn it is on, can be reset with reset();
	 * Does not store a number if there has not been a right turn before.
	 * 
	 * @param number
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public void turnLeft(int number)
	{
		myIsReset = false;
		myTurns = 2;
		if (myTurns == 2 && myActualTurned == 1)
		{
			if (number == myCombination[1])
			{
				myAttempt[1] = number;
				myIsLocked = true;
				myTurns++;
				myActualTurned = 2;
			}
		}
	}

	/**
	 * Method to reset the lock and the counter variable 
	 * for which turn you are on.
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */
	
	public void reset()
	{
		for (int i = 0; i < myAttempt.length; i++)
		{
			myAttempt[i] = 0;
		}
		myIsReset = true;
		myTurns = 1;

	}

	/**
	 * Method to return the combination of the lock.
	 * 
	 * @return the combination that was set in the constructor
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public int[] getCombination()
	{
		return myCombination;
	}

	/**
	 * Method to let the user see if the lock has been reset.
	 * 
	 * @return true if the lock has been reset, otherwise returns false
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */
	public boolean getIsReset()
	{
		if (myIsReset)
		{

			return true;
		}
		return false;
	}

	/**
	 * Method to test to see if the lock is locked.
	 * 
	 * @return true if lock is locked, otherwise returns false
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 * 
	 */
	
	public boolean isLocked()
	{
		if (myIsLocked = true)
		{
			return true;
		}
		return false;
	}

	/**
	 * Method to lock the Combo Lock, resets the combination.
	 * 
	 * @return true to lock the Combo Lock
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 * 
	 */
	public boolean lock()
	{
		myIsLocked = true;
		reset();
		return true;
	}

	/**
	 * Method to unlock the Combo Lock. Checks to see if checkUnlock()
	 * is true, if the Combo Lock is locked, if the turn is at 3, and
	 * if the turn is actually at 3, before unlocking it.
	 * 
	 * @return true if all above conditions were met, otherwise returns false
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */

	public boolean unlock()
	{
		if (checkUnlock() && myIsLocked && myTurns == 3 && myActualTurned == 3)
		{
			myIsLocked = false;
			return true;

		}
		return false;
	}
	
	/**
	 * Method to check to see if the user's attempt at index i is equal to
	 * the combination set at index i.
	 * 
	 * @return true if the above conditions are met, otherwise returns false
	 * 
	 * @author Erica Kok
	 * @author Chad Baily
	 */
	
	

	private boolean checkUnlock()
	{
		boolean match = true;
		for (int i = 0; i < myAttempt.length; i++)
		{
			match &= myAttempt[i] == myCombination[i];
		}

		return match;
	}

}